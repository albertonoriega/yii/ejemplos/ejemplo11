<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ejercicio1".
 *
 * @property string $nombre
 * @property string|null $direccion
 * @property int|null $edad
 * @property string|null $fecha
 */
class Ejercicio1 extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ejercicio1';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['edad'], 'integer'],
            [['fecha'], 'safe'],
            [['nombre'], 'string', 'max' => 100],
            [['direccion'], 'string', 'max' => 200],
            [['nombre'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nombre' => 'Nombre Completo',
            'direccion' => 'Dirección',
            'edad' => 'Edad',
            'fecha' => 'Fecha de entrada',
        ];
    }
}

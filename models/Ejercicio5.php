<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ejercicio5".
 *
 * @property int $id
 * @property string|null $foto
 */
class Ejercicio5 extends \yii\db\ActiveRecord
{
    
    private string $etiquetaImg;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ejercicio5';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['foto'],'file','skipOnEmpty' => false, 'extensions' => 'jpg,png'],
            // con skiponEmpty => false haces que sea obligatorio subir una foto
            // con extensions delimitamos el tipo de archivo que se puede subir
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'foto' => 'Foto',
        ];
    }
    
    // Este método me coloca la imagen en la carpeta imgs de web y además coloca como nombre de la imagen su id+nombre
     public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
        
        // la funcion iconv la utilizo para que saveAs no me de problemas con las tildes y ñ
        $this->foto->saveAs('imgs/' . $this->id . '_' . iconv('UTF-8','ISO-8859-1',$this->foto->name), false); // false, si la foto existe, no sube la nueva para no destruir la que está
        // La foto no es objeto => lo hemos convertido a texto para guardarla en la base de datos
        $this->foto = $this->id . '_' . iconv('UTF-8','ISO-8859-1', $this->foto->name);
        // Actualizamos el nombre de la foto de la base de datos
        $this->updateAttributes(["foto"]);
        
    }
    
    public function beforeValidate() {
               parent::beforeValidate();
        // A la propiedad foto del modelo la convertimos en un fichero
        $this->foto = \yii\web\UploadedFile::getInstance($this, 'foto');
        return true;
     
    }
    
    // Hacemos que el modelo devuelva la foto como etiqueta
    public function getEtiquetaImg(): string {
        return \yii\helpers\Html::img(
            '@web/imgs/'.$this->foto, [
                ['class'=> 'img-thumbnail col-lg-4']
            ]
        );
    }


}

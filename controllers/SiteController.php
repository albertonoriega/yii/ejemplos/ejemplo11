<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionEjercicio1() {
        // creo un modelo vacio
        $model = new \app\models\Ejercicio1();
        // compruebo si vengo del formulario
        if ($model->load(Yii::$app->request->post())) {
            // compruebo los datos
            if ($model->validate()) {
                // grabar los datos en la tabla
                $model->save();
                // Creamos un dataProvider para mostrar todos los datos de la tabla en un Gridview
                $dataProvider = new \yii\data\ActiveDataProvider([
                    "query" => \app\models\Ejercicio1::find()
                ]);
                // En la vista mostramos el dato guardado en la tabla y todos los datos de la tabla
                return $this->render('solucion1',[
                    'model' => $model,
                    'dataProvider' => $dataProvider,
                ]);
            }
        }
        
        // muestra el formulario para introducir un registro
        return $this->render('ejercicio1', [
                    'model' => $model,
        ]);
    }
    
    public function actionEjercicio1crud() {
        $dataProvider = new \yii\data\ActiveDataProvider([
                    "query" => \app\models\Ejercicio1::find()
                ]);
        
        return $this->render('ejercicio1listar',[
                    'dataProvider' => $dataProvider,
                ]);
    }
    
    public function actionEjercicio1crear() {
        // Creamos un modelo vacio
        $model = new \app\models\Ejercicio1();
        
        if($model->load(Yii::$app->request->post())) {
            //Graba los datos introducidos en la base de datos
            $model->save();
            
            return $this->render('ejercicio1ver',[
                "model" => $model,
            ]);
        }
        return $this->render('ejercicio1',[
            "model" => $model,
        ]); 
        
    }
    
    public function actionView($id) {
        //Con ActiveRecord
        $model = \app\models\Ejercicio1::find() // encontrar un valor de la tabla
                ->where(['nombre'=>$id]) // condicion de filtrado
                ->one();  // ejecuta la consulta. En SQL select * from ejercicio1 where nombre='$id'
        
        // Otra forma, con findOne, lo mismo que lo de arriba pero de una tirada
        $model = \app\models\Ejercicio1::findOne(['nombre'=>$id]);
        
        // Realizar la consulta con DAO (Sin ActiveRecord)
        // $model  = Yii::$app->db->createCommand("select * from ejercicio1 where nombre='$id'")->queryOne();
        
        return $this->render('ejercicio1ver',[
                "model" => $model,
            ]);
    }
    
    public function actionDelete($id) {
        
        $model = \app\models\Ejercicio1::findOne(['nombre' => $id]);
        
        $model->delete();
        
        return $this->redirect('ejercicio1crud');
    }
    
    public function actionUpdate($id) {
        $model = \app\models\Ejercicio1::findOne(['nombre' => $id]);
        
        // Si se han introducido datos nuevos 
        if($model->load(Yii::$app->request->post())){
            // Guarda y actualiza los cambios
            $model->save();
            // muestra los cambios
            return $this->render('ejercicio1ver',[
                "model" => $model,
            ]);
        }
        return $this->render('ejercicio1',[
            "model" => $model,
        ]);
    }

}

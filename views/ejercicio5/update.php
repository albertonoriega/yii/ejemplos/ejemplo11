<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Ejercicio5 $model */

$this->title = 'Update Ejercicio5: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Ejercicio5s', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ejercicio5-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

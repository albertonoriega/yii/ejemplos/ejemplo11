<?php

use app\models\Ejercicio5;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Ejercicio5s';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ejercicio5-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Ejercicio5', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a(
                'Imprimir',
                ['imprimir'],
                ['class' => 'btn btn-primary']) 
        ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            //'foto',
            // Mostramos la foto con el getter que hemos creado en el modelo
            [
                'attribute'=> 'foto',
                'format' => 'raw',
                'value' => function ($model){
                    return $model->etiquetaImg;
                },
                'options' => [
                    'class' => 'col-lg-4',
                ],
            ],
            // Mostramos la foto         
//            [
//                "attribute" => "foto",
//                "format"=> "raw",
//                "value" => function ($model){
//                    if (isset($model->foto) && $model->foto!=""){
//                        return Html::img("@web/imgs/". $model->foto,
//                            ["class" => "img-thumbnail"],
//                            
//                        );
//                    }
//                    
//                }
//            ],
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Ejercicio5 $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>

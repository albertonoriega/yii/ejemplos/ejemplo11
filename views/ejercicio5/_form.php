<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Ejercicio5 $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="ejercicio5-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
        // File input cute de HTML
        //echo $form->field($model, 'foto')->fileInput() 
    
    
        // Widget de kartik ActiveForm y modelo
        echo $form->field($model, 'foto')->widget(kartik\file\FileInput::classname(), [
        'options' => ['accept' => 'image/*'],
]);
    
    ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

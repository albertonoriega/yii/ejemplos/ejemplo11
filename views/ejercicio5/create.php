<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Ejercicio5 $model */

$this->title = 'Create Ejercicio5';
$this->params['breadcrumbs'][] = ['label' => 'Ejercicio5s', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ejercicio5-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

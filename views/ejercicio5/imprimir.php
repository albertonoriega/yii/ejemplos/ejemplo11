<?php
    echo \yii\grid\GridView::widget([
            "dataProvider" => $dataProvider,
            "layout" =>'{items}',
            "columns" =>[
                "id",
                [
                    "attribute" => "foto",
                    "content" => function($model){
                        return yii\helpers\Html::img(
                                '@web/imgs/'.$model->foto,
                                [
                                    'style' => 'width:6cm',
                                ],
                        );
                    }
                ],
            ]
    ]);
?>

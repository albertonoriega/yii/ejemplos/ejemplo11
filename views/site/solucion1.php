<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

?>


<h2>Registro almacenado correctamente</h2>

<?php

echo DetailView::widget([
    "model" => $model,
]);

echo Html::a("Nuevo registro",['site/ejercicio1'],["class" => "btn btn-primary"]);

?>

<h2>TODOS LOS REGISTROS</h2>

<?php

echo GridView::widget([
    "dataProvider" => $dataProvider,
]);


<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Ejercicio1 $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="ejercicio1-form">

    <?php $form = ActiveForm::begin(); 

    echo $form->field($model, 'nombre')->textInput([
            'placeholder' => 'Introduce tu nombre'
        ]);
        echo $form->field($model, 'direccion')->textarea(['rows'=>8]); 
        echo $form->field($model, 'edad')->input('number'); 
        echo $form->field($model, 'fecha')->input('date'); 
        
        ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

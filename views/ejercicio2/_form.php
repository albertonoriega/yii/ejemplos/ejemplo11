<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Ejercicio2 $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="ejercicio2-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(
                        ['maxlength' => true, 'placeholder' => 'Escriba su nombre',]
                ) ?>

    <?= $form->field($model, 'poblacion')->dropDownList($model->poblaciones(),
                        ['prompt' => 'Seleccione una poblacion',] // conseguimos que en la lista te salga por defecto un mensaje 
                        ) ?>

    <?= $form->field($model, 'color')->listBox($model->colores()) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use app\models\Ejercicio2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Ejercicio2s';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ejercicio2-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Ejercicio2', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'nombre',
            'poblacion',
            'color',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Ejercicio2 $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'nombre' => $model->nombre]);
                 }
            ],
        ],
    ]); ?>


</div>

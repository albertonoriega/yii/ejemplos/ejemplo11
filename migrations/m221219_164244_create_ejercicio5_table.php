<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%ejercicio5}}`.
 */
class m221219_164244_create_ejercicio5_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%ejercicio5}}', [
            'id' => $this->primaryKey(),
            'foto' => $this->string(100),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%ejercicio5}}');
    }
}
